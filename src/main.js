import Vue from 'vue'
import App from './App.vue'
// import Ninjas from './Ninjas.vue'

// This is global declaration of component
// We will comment below to declare it locally
// Vue.component('ninjas-component', Ninjas);

// We will create a new event bus
export const bus = new Vue();

new Vue({
  el: '#app',
  render: h => h(App)
})
